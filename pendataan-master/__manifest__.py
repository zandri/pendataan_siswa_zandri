{
	'author': 'Syukriatul Zandri',
	'name': 'Pendataan murid',
	'category': 'education',
	'summary': """module ini akan menyimpan data murid""",
        'version': '0.0.1',
	'website': 'www.zANDRi',
	'company': 'SMK N 2 GUGUAK',
	'description': 'modul pendataan murid sederhana',
	'depends': ['base'],
	'data': [
		'security/security.xml',
        	'security/ir.model.access.csv',
		'views/halmen.xml'
	], 
	'images': ['static/description/icon.png'],
    	'application': True,
}
	

